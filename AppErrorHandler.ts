import { Alert } from 'react-native';
import { setJSExceptionHandler } from 'react-native-exception-handler';
import RNRestart from 'react-native-restart';

import { ErrorParserService } from './src/services/ErrorParserService';

const errorHandler: any = (error, isFatal) => {
  // This is your custom global error handler
  // You do stuff like show an error dialog
  // or hit google analytics to track crashes
  // or hit a custom api to inform the dev team.

  console.log(error);

  if (isFatal) {
    Alert.alert('Error', ErrorParserService.parseErrorToMessage(error), [
      {
        text: 'Restart',
        onPress: () => {
          RNRestart.Restart();
        },
      },
      {
        text: 'Close',
      },
    ]);
  }
};

// registering the error handler (maybe u can do this in the index.android.js or index.ios.js)
setJSExceptionHandler(errorHandler, false);
