# React Native Starter

## How We Write Codes

### Typescript
1. https://www.geeksforgeeks.org/difference-between-typescript-and-javascript/
2. https://www.cheatography.com/gregfinzer/cheat-sheets/typescript/

### React Native 
1. https://facebook.github.io/react-native/

### Native Base
1. http://nativebase.io

### MobX
1. https://mobx.js.org


## We Are One
The `tslint.json` are helping our development code style to be the same


## Visual Studio Code Required Plugins
1. https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode/items?itemName=ms-vscode.vscode-typescript-tslint-plugin
2. https://marketplace.visualstudio.com/items?itemName=rbbit.typescript-hero
3. https://marketplace.visualstudio.com/items?itemName=vsmobile.vscode-react-native
4. https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons
5. https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer
6. https://marketplace.visualstudio.com/items?itemName=stringham.move-ts
7. https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight

## How To Start Develop

### Run Project

Start an Android emulator or connect your phone by turning on Android Debugging feature, and then start the project
```bash
npm run android
```

### Build Project

#### Staging/Development
Verify .env.staging file, make sure the configurations match the exact requirements for staging mode (e.g: endpoint url, etc), and then start build the APK:
```bash
npm run buildAPKStaging
```

The APK will be ready inside `android/app/build/outputs/apk`

#### Production
Verify .env.production file, make sure the configurations match the exact requirements for production mode (e.g: endpoint url, etc), and then start build the APK:
```bash
npm run buildAPKProd
```

The APK will be ready inside `android/app/build/outputs/apk`