import './AppErrorHandler';
import 'reflect-metadata';

import { Root, StyleProvider } from 'native-base';
import React from 'react';

import { AppContainer } from './AppNavigator';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import { LoadingComponent } from './src/components/LoadingComponent';
import { NavigationStore } from './src/stores/NavigationStore';
import { DependencyContainer } from './src/utils/DI';

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
export class App extends React.Component<{}, {}> {
  public navigationStore = DependencyContainer.get(NavigationStore);

  public render() {
    console.disableYellowBox = true;

    return (
      <StyleProvider style={getTheme(material)}>
        <Root>
          <LoadingComponent />
          <AppContainer
            ref={(ref) => {
              this.navigationStore.setNavigator(ref);
            }}
          />
        </Root>
      </StyleProvider>
    );
  }
}
