import * as React from 'react';
import { createAppContainer, createDrawerNavigator, createStackNavigator } from 'react-navigation';

import { SidebarComponent } from './src/components/SidebarComponent';
import { DashboardContainer } from './src/containers/DashboardContainer';
import { WelcomeContainer } from './src/containers/WelcomeContainer';

export const AppNavigator = createStackNavigator(
  {
    Drawer: createDrawerNavigator(
      {
        WelcomeContainer: { screen: WelcomeContainer },
        DashboardContainer: { screen: DashboardContainer },
      },
      {
        initialRouteName: 'WelcomeContainer',
        contentComponent: (props) => <SidebarComponent {...props} />,
      },
    ),
  },
  {
    initialRouteName: 'Drawer',
    headerMode: 'none',
  },
);

export const AppContainer = createAppContainer(AppNavigator);
