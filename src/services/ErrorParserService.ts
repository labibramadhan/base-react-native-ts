import * as ErrorStackParser from 'error-stack-parser';
import { forEach, has, isArray, isObject, isString } from 'lodash';

export class ErrorParserService {
  public static parseErrorToMessages(error: any, errorMessages: string[] = []) {
    if (error) {
      if (error.response && error.response.data) {
        error = error.response.data;
      }

      if (has(error, 'errors') && isArray(error.errors)) {
        error.errors.forEach((errorItem) => {
          ErrorParserService.parseErrorToMessages(errorItem, errorMessages);
        });
      } else if (isArray(error)) {
        forEach(error, (errorItem) => {
          ErrorParserService.parseErrorToMessages(errorItem, errorMessages);
        });
      } else if (error.message) {
        errorMessages.push(error.message);
      } else if (isObject(error) && error.hasOwnProperty('toString')) {
        errorMessages.push(error.toString());
      } else if (isObject(error) && (has(error, 'stack') || has(error, 'stacktrace'))) {
        const stackFrames = ErrorStackParser.parse(error as any);
        errorMessages.push(...stackFrames.map((stackFrame) => `${stackFrame.toString()}`));
      } else if (isObject(error)) {
        errorMessages.push(JSON.stringify(error, null, 2));
      } else if (isString(error)) {
        errorMessages.push(error);
      }
    }

    return errorMessages;
  }

  public static parseErrorToMessage(error: any) {
    return ErrorParserService.parseErrorToMessages(error).join('\n');
  }
}
