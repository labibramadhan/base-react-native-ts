import { computed, observable } from 'mobx';

import { Injectable } from '../utils/DI';

@Injectable()
export class LoadingStore {
  @observable public loadings: boolean[] = [];

  @computed public get isLoading(): boolean {
    return this.loadings.filter((loading) => loading === true).length > 0;
  }

  public createLoading() {
    let disposed = false;
    return {
      show: () => {
        this.loadings.push(true);
      },
      dispose: () => {
        if (!disposed) {
          this.loadings.splice(0, 1);
          disposed = true;
        }
      },
    };
  }
}
