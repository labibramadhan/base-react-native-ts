import { action, observable } from 'mobx';
import { DrawerActions, NavigationActions, StackActions } from 'react-navigation';

import { Injectable } from '../utils/DI';

@Injectable()
export class NavigationStore {
  @observable public navigator: any;

  @action.bound public setNavigator(navigatorRef) {
    this.navigator = navigatorRef;
  }

  @action.bound public back() {
    this.navigator.dispatch(NavigationActions.back());
  }

  @action.bound public pop() {
    this.navigator.dispatch(StackActions.pop({ n: 1 }));
  }

  @action.bound public replace(routeName, params?, action?) {
    setTimeout(() => {
      this.navigator.dispatch(StackActions.replace({ routeName, params, action }));
    }, 1);
  }

  @action.bound public navigate(routeName, params?, action?) {
    setTimeout(() => {
      this.navigator.dispatch(NavigationActions.navigate({ routeName, params, action }));
    }, 1);
  }

  @action.bound public openDrawer() {
    this.navigator.dispatch(DrawerActions.openDrawer());
  }

  @action.bound public closeDrawer() {
    this.navigator.dispatch(DrawerActions.closeDrawer());
  }

  @action.bound public toggleDrawer() {
    this.navigator.dispatch(DrawerActions.toggleDrawer());
  }
}
