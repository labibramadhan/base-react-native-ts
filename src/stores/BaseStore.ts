import { Alert } from 'react-native';
import { Observable, of } from 'rxjs';
import { catchError, finalize, switchMapTo, tap } from 'rxjs/operators';

import { ErrorParserService } from '../services/ErrorParserService';
import { DependencyContainer, Injectable } from '../utils/DI';
import { LoadingStore } from './LoadingStore';


export class BaseStoreObservableWrapOptions {
  public wrapLoading?: boolean = true;
  public wrapErrorAlert?: boolean = true;
}

@Injectable()
export class BaseStore {
  public loadingStore = DependencyContainer.get(LoadingStore);

  public wrapObservable<TResult = any>(
    targetObservable,
    wrapOptions: Partial<BaseStoreObservableWrapOptions> = new BaseStoreObservableWrapOptions(),
  ): Observable<TResult> {
    let wrappedObservable = targetObservable;

    if (wrapOptions.wrapLoading) {
      const loading = this.loadingStore.createLoading();
      wrappedObservable = of(true).pipe(
        tap(() => loading.show()),
        switchMapTo(wrappedObservable),
        finalize(() => {
          loading.dispose();
        }),
      );
    }

    if (wrapOptions.wrapErrorAlert) {
      wrappedObservable = wrappedObservable.pipe(
        catchError((error) => {
          const errorMessage = ErrorParserService.parseErrorToMessage(error);
          Alert.alert('Error', errorMessage);
          throw error;
        }),
      );
    }

    return wrappedObservable;
  }
}
