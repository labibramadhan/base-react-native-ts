import { Container, Content } from 'native-base';
import React from 'react';

import { DashboardComponent } from '../components/DashboardComponent';
import { HeaderComponent } from '../components/HeaderComponent';

export class DashboardContainer extends React.Component {
  public render() {
    return (
      <Container>
        <HeaderComponent drawer title="Dashboard" />
        <Content padder>
          <DashboardComponent />
        </Content>
      </Container>
    );
  }
}
