import { Container, Content } from 'native-base';
import React from 'react';

import { HeaderComponent } from '../components/HeaderComponent';
import { WelcomeComponent } from '../components/WelcomeComponent';

export class WelcomeContainer extends React.Component {
  public render() {
    return (
      <Container>
        <HeaderComponent drawer title="Welcome" />
        <Content padder>
          <WelcomeComponent />
        </Content>
      </Container>
    );
  }
}
