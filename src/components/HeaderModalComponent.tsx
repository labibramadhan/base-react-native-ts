import { Body, Button, Header, Icon, Left, Right, Title } from 'native-base';
import * as React from 'react';
import { BaseReactComponent } from './BaseReactComponent';

export class HeaderModalComponent extends BaseReactComponent<
  HeaderModalComponent,
  {
    title: string;
    onBackPress: () => void;
  },
  {}
> {
  public render() {
    return (
      <Header>
        <Left>
          <Button transparent onPress={this.props.onBackPress}>
            <Icon name='arrow-back' />
          </Button>
        </Left>
        <Body>
          <Title>{this.props.title}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}
