import { observer } from 'mobx-react';
import { Badge, Icon, Left, List, ListItem, Right, Text } from 'native-base';
import * as React from 'react';
import { NavigationActions } from 'react-navigation';

import { NavigationStore } from '../stores/NavigationStore';
import { pl2 } from '../utils/Css';
import { DependencyContainer } from '../utils/DI';
import { BaseReactComponent } from './BaseReactComponent';
import { SidebarMenuComponentStyle } from './SidebarMenuComponentStyle';

@observer
export class SidebarMenuComponent extends BaseReactComponent<
  SidebarMenuComponent,
  {
    onCloseDrawer: () => void;
  },
  {}
> {
  public navigationStore = DependencyContainer.get(NavigationStore);

  menus = [
    {
      label: 'Welcome',
      route: 'WelcomeContainer',
    },
    {
      label: 'Dashboard',
      route: 'DashboardContainer',
    },
  ];

  public onMenuItemPress(menu) {
    if (menu.route) {
      this.navigationStore.replace('Drawer', {}, NavigationActions.navigate({ routeName: menu.route }));
    } else if (menu.command) {
      menu.command();
    }

    this.props.onCloseDrawer();
  }

  public render() {
    return (
      <List
        dataArray={this.menus}
        renderRow={(menu) => (
          <ListItem button noBorder onPress={() => this.onMenuItemPress(menu)}>
            <Left>
              {Boolean(menu.icon) && <Icon active name={menu.icon} style={{ color: '#777', fontSize: 26, width: 30 }} />}
              <Text style={[SidebarMenuComponentStyle.text, pl2]}>{menu.label}</Text>
            </Left>
            {Boolean(menu.badgeCount) && (
              <Right style={{ flex: 1 }}>
                <Badge
                  style={{
                    borderRadius: 3,
                    height: 25,
                    width: 72,
                    backgroundColor: menu.badgeBackgroundColor,
                  }}
                >
                  <Text style={SidebarMenuComponentStyle.badgeText}>{menu.badgeCount}</Text>
                </Badge>
              </Right>
            )}
          </ListItem>
        )}
      />
    );
  }
}
