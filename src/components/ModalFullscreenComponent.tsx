import * as React from 'react';
import { View } from 'react-native';
import Modal, { ModalProps } from 'react-native-modal';

import { flexAuto, m0 } from '../utils/Css';
import { BaseReactComponent } from './BaseReactComponent';

export class ModalFullscreenComponent extends BaseReactComponent<ModalFullscreenComponent, ModalProps, {}> {
  public render() {
    return (
      <Modal
        presentationStyle="fullScreen"
        backdropOpacity={0}
        style={[m0]}
        {...this.props}
      >
        <View style={[flexAuto]}>
          {this.props.children}
        </View>
      </Modal>
    );
  }
}
