export const SidebarComponentStyle = {
  drawerCover: {
    alignSelf: 'stretch',
    backgroundColor: '#d0021b',
  },
  drawerImage: {
    width: 210,
    height: 75,
    resizeMode: 'contain',
  },
};
