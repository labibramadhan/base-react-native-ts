import { action } from 'mobx';
import { observer } from 'mobx-react';
import { Body, Button, Header, Icon, Left, NativeBase, Right, Title } from 'native-base';
import * as React from 'react';
import { View } from 'react-native';

import { NavigationStore } from '../stores/NavigationStore';
import { mr4 } from '../utils/Css';
import { DependencyContainer } from '../utils/DI';
import { BaseReactComponent } from './BaseReactComponent';

@observer
export class HeaderComponent extends BaseReactComponent<
  HeaderComponent,
  {
    title: string;
    drawer?: boolean;
    customBackIconPress?: () => void;
    customLeft?: any;
    customRight?: any;
    disableLeft?: boolean;
    headerProps?: NativeBase.Header;
  },
  {}
> {
  public navigationStore = DependencyContainer.get(NavigationStore);

  @action.bound onBackIconPress() {
    if (this.props.customBackIconPress) {
      this.props.customBackIconPress();
    } else {
      this.navigationStore.back();
    }
  }

  public render() {
    const { drawer, disableLeft, customLeft, customRight } = this.props;

    return (
      <Header {...this.props.headerProps}>
        {!Boolean(disableLeft) && (
          <Left>
            {!Boolean(customLeft) && Boolean(drawer) && (
              <Button transparent onPress={this.navigationStore.toggleDrawer}>
                <Icon name="menu" />
              </Button>
            )}

            {!Boolean(customLeft) && !Boolean(drawer) && (
              <Button transparent onPress={this.onBackIconPress}>
                <Icon name="arrow-back" />
              </Button>
            )}

            {Boolean(customLeft) && customLeft}
          </Left>
        )}
        {Boolean(disableLeft) && <View style={[mr4]} />}

        <Body>
          <Title>{this.props.title}</Title>
        </Body>

        <Right>{Boolean(customRight) && customRight}</Right>
      </Header>
    );
  }
}
