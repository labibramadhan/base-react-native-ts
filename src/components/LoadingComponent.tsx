import { observer } from 'mobx-react';
import { Text, View } from 'native-base';
import * as React from 'react';
import { ActivityIndicator, Modal } from 'react-native';

import { LoadingStore } from '../stores/LoadingStore';
import { DependencyContainer } from '../utils/DI';
import { BaseReactComponent } from './BaseReactComponent';
import { LoadingComponentStyle } from './LoadingComponentStyle';

@observer
export class LoadingComponent extends BaseReactComponent<LoadingComponent, {}, {}> {
  loadingStore = DependencyContainer.get(LoadingStore);

  render() {
    return (
      <Modal
        transparent
        visible={this.loadingStore.isLoading}
      >
        <View style={LoadingComponentStyle.container}>
          <View style={LoadingComponentStyle.layout}>
            <ActivityIndicator color={'#000000'} size="small" />
            <Text style={LoadingComponentStyle.text}>Loading...</Text>
          </View>
        </View>
      </Modal>
    )
  }
}