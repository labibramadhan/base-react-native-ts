import * as React from 'react';
import QRCodeScanner, { Event as QRCodeScannerEvent } from 'react-native-qrcode-scanner';
import { BaseReactComponent } from './BaseReactComponent';

export class QRCodeScannerComponent extends BaseReactComponent<
  QRCodeScannerComponent,
  {
    onSuccess: (result: QRCodeScannerEvent) => void;
  },
  {}
> {
  public render() {
    return <QRCodeScanner onRead={this.props.onSuccess} />;
  }
}
