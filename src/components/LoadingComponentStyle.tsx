import { StyleSheet } from 'react-native';

export const LoadingComponentStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00000050'
  },
  layout: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 3,
    elevation: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  text: {
    paddingLeft: 20,
    color: '#000000'
  },
});