import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import * as React from 'react';

interface BaseReactComponentProps<TClass> {
  ref?: (ref: TClass) => void;
}

@observer
export class BaseReactComponent<TClass = {}, P = {}, S = {}, SS = any> extends React.Component<
  P & BaseReactComponentProps<TClass>,
  S,
  SS
> {
  @observable public elementsVisibility = new Map();

  public isElementVisible(key: string) {
    return this.elementsVisibility.get(key) === true;
  }

  @action.bound public setElementVisible(key: string) {
    this.elementsVisibility.set(key, true);
  }

  @action.bound public setElementInvisible(key: string) {
    this.elementsVisibility.set(key, false);
  }

  componentDidMount() {
    if (this.props.ref) {
      this.props.ref(this as any);
    }
  }
}
