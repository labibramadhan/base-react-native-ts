import { observable } from 'mobx';
import { observer } from 'mobx-react';
import { Button, Text, View } from 'native-base';
import React from 'react';

@observer
export class WelcomeComponent extends React.Component {
  @observable counter = 0;

  count() {
    this.counter = this.counter + 1;
  }

  public render() {
    return (
      <View>
        <Text>Welcome! {this.counter}</Text>
        <Button onPress={() => this.count()}>
          <Text>Increase Counter</Text>
        </Button>
      </View>
    );
  }
}
