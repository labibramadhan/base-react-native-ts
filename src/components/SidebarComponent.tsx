import { observer } from 'mobx-react';
import { Container, Content } from 'native-base';
import * as React from 'react';
import { Image, View } from 'react-native';

import Images from '../assets/images';
import { NavigationStore } from '../stores/NavigationStore';
import { p4, pb8, pl6, pt2, pt8 } from '../utils/Css';
import { DependencyContainer } from '../utils/DI';
import { BaseReactComponent } from './BaseReactComponent';
import { SidebarComponentStyle } from './SidebarComponentStyle';
import { SidebarMenuComponent } from './SidebarMenuComponent';

@observer
export class SidebarComponent extends BaseReactComponent<SidebarComponent> {
  public navigationStore = DependencyContainer.get(NavigationStore);

  public render() {
    return (
      <Container>
        <Content style={{ flex: 1, backgroundColor: '#fff', top: -1 }}>
          <View style={[SidebarComponentStyle.drawerCover, p4, pl6, pt8, pb8]}>
            <Image square style={SidebarComponentStyle.drawerImage} source={Images.logoSidebar} />
          </View>

          <View style={[pt2]}>
            <SidebarMenuComponent onCloseDrawer={this.navigationStore.closeDrawer} />
          </View>
        </Content>
      </Container>
    );
  }
}
