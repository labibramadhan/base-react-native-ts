import * as React from 'react';
import { View } from 'react-native';

export class EmptyViewComponent extends React.Component<{}, {}> {
  render() {
    return (
      <View />
    );
  }
}