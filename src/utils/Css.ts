import { GREY } from 'react-native-material-color';

export const borderTopNone = { borderTopWidth: 0 };
export const borderRightNone = { borderRightWidth: 0 };
export const borderBottomNone = { borderBottomWidth: 0 };
export const borderLeftNone = { borderLeftWidth: 0 };
export const borderNone = {
  borderTopWidth: 0,
  borderRightWidth: 0,
  borderBottomWidth: 0,
  borderLeftWidth: 0,
};

export const borderBoottomGrey1 = {
  borderBottomColor: '#e0e0e0',
  borderBottomWidth: 1,
};

export const borderTopGrey1 = {
  borderTopColor: '#e0e0e0',
  borderTopWidth: 1,
};

export const colorWhite = { color: 'white' };
export const colorBlack = { color: 'black' };

// flexbox
export const flexRow = { flexDirection: 'row' };
export const flexRowReverse = { flexDirection: 'row-reverse' };
export const flexColumn = { flexDirection: 'column' };
export const flexColumnReverse = { flexDirection: 'column-reverse' };

export const flex = (size: number) => ({ flex: size });
export const flexAuto = { flex: 1 };
export const flexNoShrink = { flex: 0 };

export const flexWrap = { flexWrap: 'wrap' };

export const itemsStart = { alignItems: 'flex-start' };
export const itemsEnd = { alignItems: 'flex-end' };
export const itemsCenter = { alignItems: 'center' };
export const itemsStretch = { alignItems: 'stretch' };
export const itemsBaseline = { alignItems: 'baseline' };

export const selfAuto = { alignSelf: 'auto' };
export const selfStart = { alignSelf: 'flex-start' };
export const selfEnd = { alignSelf: 'flex-end' };
export const selfCenter = { alignSelf: 'center' };
export const selfStretch = { alignSelf: 'stretch' };
export const selfBaseline = { alignSelf: 'baseline' };

export const justifyStart = { justifyContent: 'flex-start' };
export const justifyEnd = { justifyContent: 'flex-end' };
export const justifyCenter = { justifyContent: 'center' };
export const justifyBetween = { justifyContent: 'space-between' };
export const justifyAround = { justifyContent: 'space-around' };
export const justifyEvenly = { justifyContent: 'space-evenly' };

export const contentStart = { alignContent: 'flex-start' };
export const contentEnd = { alignContent: 'flex-end' };
export const contentCenter = { alignContent: 'center' };
export const contentStretch = { alignContent: 'stretch' };
export const contentBetween = { alignContent: 'space-between' };
export const contentAround = { alignContent: 'space-around' };

// hide
export const hide = { display: 'none' };

// layout
export const overflowVisible = { overflow: 'visible' };
export const overflowHidden = { overflow: 'hidden' };
export const overflowScroll = { overflow: 'scroll' };

// position
export const absolute = { position: 'absolute' };
export const relative = { position: 'relative' };

export const top0 = { top: 0 };
export const right0 = { right: 0 };
export const bottom0 = { bottom: 0 };
export const left0 = { left: 0 };

export const z1 = { zIndex: 1 };
export const z2 = { zIndex: 2 };
export const z3 = { zIndex: 3 };
export const z4 = { zIndex: 4 };

// type-scale
export const size10 = { fontSize: 10 };
export const size11 = { fontSize: 11 };
export const size12 = { fontSize: 12 };
export const size13 = { fontSize: 13 };
export const size14 = { fontSize: 14 };
export const size15 = { fontSize: 15 };
export const size16 = { fontSize: 16 };
export const size17 = { fontSize: 17 };
export const size18 = { fontSize: 18 };
export const size19 = { fontSize: 19 };
export const size20 = { fontSize: 20 };
export const size21 = { fontSize: 21 };
export const size22 = { fontSize: 22 };
export const size23 = { fontSize: 23 };
export const size24 = { fontSize: 24 };
export const size25 = { fontSize: 25 };
export const size26 = { fontSize: 26 };
export const size27 = { fontSize: 27 };
export const size28 = { fontSize: 28 };
export const size29 = { fontSize: 29 };
export const size30 = { fontSize: 30 };

// typography
export const italic = { fontStyle: 'italic' };

export const bold = { fontWeight: 'bold' };
export const weight1 = { fontWeight: '100' };
export const weight2 = { fontWeight: '200' };
export const weight3 = { fontWeight: '300' };
export const weight4 = { fontWeight: '400' };
export const weight5 = { fontWeight: '500' };
export const weight6 = { fontWeight: '600' };
export const weight7 = { fontWeight: '700' };
export const weight8 = { fontWeight: '800' };
export const weight9 = { fontWeight: '900' };

export const textLeft = { textAlign: 'left' };
export const textRight = { textAlign: 'right' };
export const textCenter = { textAlign: 'center' };
export const justify = { textAlign: 'justify' };

// @deprecate
// https://facebook.github.io/react-native/docs/layout-props#flex
export const flexAutoGrow = { flexGrow: 1 };

const marginPaddingModifier = 4;

export const mt0 = { marginTop: marginPaddingModifier * 0 };
export const mt1 = { marginTop: marginPaddingModifier * 1 };
export const mt2 = { marginTop: marginPaddingModifier * 2 };
export const mt3 = { marginTop: marginPaddingModifier * 3 };
export const mt4 = { marginTop: marginPaddingModifier * 4 };
export const mt5 = { marginTop: marginPaddingModifier * 5 };
export const mt6 = { marginTop: marginPaddingModifier * 6 };
export const mt7 = { marginTop: marginPaddingModifier * 7 };
export const mt8 = { marginTop: marginPaddingModifier * 8 };
export const mt9 = { marginTop: marginPaddingModifier * 9 };
export const mt10 = { marginTop: marginPaddingModifier * 10 };

export const mr0 = { marginRight: marginPaddingModifier * 0 };
export const mr1 = { marginRight: marginPaddingModifier * 1 };
export const mr2 = { marginRight: marginPaddingModifier * 2 };
export const mr3 = { marginRight: marginPaddingModifier * 3 };
export const mr4 = { marginRight: marginPaddingModifier * 4 };
export const mr5 = { marginRight: marginPaddingModifier * 5 };
export const mr6 = { marginRight: marginPaddingModifier * 6 };
export const mr7 = { marginRight: marginPaddingModifier * 7 };
export const mr8 = { marginRight: marginPaddingModifier * 8 };
export const mr9 = { marginRight: marginPaddingModifier * 9 };
export const mr10 = { marginRight: marginPaddingModifier * 10 };

export const ml0 = { marginLeft: marginPaddingModifier * 0 };
export const ml1 = { marginLeft: marginPaddingModifier * 1 };
export const ml2 = { marginLeft: marginPaddingModifier * 2 };
export const ml3 = { marginLeft: marginPaddingModifier * 3 };
export const ml4 = { marginLeft: marginPaddingModifier * 4 };
export const ml5 = { marginLeft: marginPaddingModifier * 5 };
export const ml6 = { marginLeft: marginPaddingModifier * 6 };
export const ml7 = { marginLeft: marginPaddingModifier * 7 };
export const ml8 = { marginLeft: marginPaddingModifier * 8 };
export const ml9 = { marginLeft: marginPaddingModifier * 9 };
export const ml10 = { marginLeft: marginPaddingModifier * 10 };

export const mb0 = { marginBottom: marginPaddingModifier * 0 };
export const mb1 = { marginBottom: marginPaddingModifier * 1 };
export const mb2 = { marginBottom: marginPaddingModifier * 2 };
export const mb3 = { marginBottom: marginPaddingModifier * 3 };
export const mb4 = { marginBottom: marginPaddingModifier * 4 };
export const mb5 = { marginBottom: marginPaddingModifier * 5 };
export const mb6 = { marginBottom: marginPaddingModifier * 6 };
export const mb7 = { marginBottom: marginPaddingModifier * 7 };
export const mb8 = { marginBottom: marginPaddingModifier * 8 };
export const mb9 = { marginBottom: marginPaddingModifier * 9 };
export const mb10 = { marginBottom: marginPaddingModifier * 10 };

export const m0 = Object.assign({}, mt0, mr0, ml0, mb0);
export const m1 = Object.assign({}, mt1, mr1, ml1, mb1);
export const m2 = Object.assign({}, mt2, mr2, ml2, mb2);
export const m3 = Object.assign({}, mt3, mr3, ml3, mb3);
export const m4 = Object.assign({}, mt4, mr4, ml4, mb4);
export const m5 = Object.assign({}, mt5, mr5, ml5, mb5);
export const m6 = Object.assign({}, mt6, mr6, ml6, mb6);
export const m7 = Object.assign({}, mt7, mr7, ml7, mb7);
export const m8 = Object.assign({}, mt8, mr8, ml8, mb8);
export const m9 = Object.assign({}, mt9, mr9, ml9, mb9);
export const m10 = Object.assign({}, mt10, mr10, ml10, mb10);

export const pt0 = { paddingTop: marginPaddingModifier * 0 };
export const pt1 = { paddingTop: marginPaddingModifier * 1 };
export const pt2 = { paddingTop: marginPaddingModifier * 2 };
export const pt3 = { paddingTop: marginPaddingModifier * 3 };
export const pt4 = { paddingTop: marginPaddingModifier * 4 };
export const pt5 = { paddingTop: marginPaddingModifier * 5 };
export const pt6 = { paddingTop: marginPaddingModifier * 6 };
export const pt7 = { paddingTop: marginPaddingModifier * 7 };
export const pt8 = { paddingTop: marginPaddingModifier * 8 };
export const pt9 = { paddingTop: marginPaddingModifier * 9 };
export const pt10 = { paddingTop: marginPaddingModifier * 10 };

export const pr0 = { paddingRight: marginPaddingModifier * 0 };
export const pr1 = { paddingRight: marginPaddingModifier * 1 };
export const pr2 = { paddingRight: marginPaddingModifier * 2 };
export const pr3 = { paddingRight: marginPaddingModifier * 3 };
export const pr4 = { paddingRight: marginPaddingModifier * 4 };
export const pr5 = { paddingRight: marginPaddingModifier * 5 };
export const pr6 = { paddingRight: marginPaddingModifier * 6 };
export const pr7 = { paddingRight: marginPaddingModifier * 7 };
export const pr8 = { paddingRight: marginPaddingModifier * 8 };
export const pr9 = { paddingRight: marginPaddingModifier * 9 };
export const pr10 = { paddingRight: marginPaddingModifier * 10 };

export const pl0 = { paddingLeft: marginPaddingModifier * 0 };
export const pl1 = { paddingLeft: marginPaddingModifier * 1 };
export const pl2 = { paddingLeft: marginPaddingModifier * 2 };
export const pl3 = { paddingLeft: marginPaddingModifier * 3 };
export const pl4 = { paddingLeft: marginPaddingModifier * 4 };
export const pl5 = { paddingLeft: marginPaddingModifier * 5 };
export const pl6 = { paddingLeft: marginPaddingModifier * 6 };
export const pl7 = { paddingLeft: marginPaddingModifier * 7 };
export const pl8 = { paddingLeft: marginPaddingModifier * 8 };
export const pl9 = { paddingLeft: marginPaddingModifier * 9 };
export const pl10 = { paddingLeft: marginPaddingModifier * 10 };

export const pb0 = { paddingBottom: marginPaddingModifier * 0 };
export const pb1 = { paddingBottom: marginPaddingModifier * 1 };
export const pb2 = { paddingBottom: marginPaddingModifier * 2 };
export const pb3 = { paddingBottom: marginPaddingModifier * 3 };
export const pb4 = { paddingBottom: marginPaddingModifier * 4 };
export const pb5 = { paddingBottom: marginPaddingModifier * 5 };
export const pb6 = { paddingBottom: marginPaddingModifier * 6 };
export const pb7 = { paddingBottom: marginPaddingModifier * 7 };
export const pb8 = { paddingBottom: marginPaddingModifier * 8 };
export const pb9 = { paddingBottom: marginPaddingModifier * 9 };
export const pb10 = { paddingBottom: marginPaddingModifier * 10 };

export const p0 = Object.assign({}, pt1, pr1, pl1, pb1);
export const p1 = Object.assign({}, pt1, pr1, pl1, pb1);
export const p2 = Object.assign({}, pt2, pr2, pl2, pb2);
export const p3 = Object.assign({}, pt3, pr3, pl3, pb3);
export const p4 = Object.assign({}, pt4, pr4, pl4, pb4);
export const p5 = Object.assign({}, pt5, pr5, pl5, pb5);
export const p6 = Object.assign({}, pt6, pr6, pl6, pb6);
export const p7 = Object.assign({}, pt7, pr7, pl7, pb7);
export const p8 = Object.assign({}, pt8, pr8, pl8, pb8);
export const p9 = Object.assign({}, pt9, pr9, pl9, pb9);
export const p10 = Object.assign({}, pt10, pr10, pl10, pb10);

export const fontSize13 = { fontSize: 13 };
export const fontSize14 = { fontSize: 14 };
export const fontSize15 = { fontSize: 15 };
export const fontSize16 = { fontSize: 16 };
export const fontSize17 = { fontSize: 17 };
export const fontSize18 = { fontSize: 18 };
export const fontSize19 = { fontSize: 19 };
export const fontSize20 = { fontSize: 20 };

export const listItemSelectedBgGrey = {
  backgroundColor: GREY[300],
};