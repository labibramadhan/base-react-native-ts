import { Container, Service, Token } from 'typedi';

export class CannotInjectError extends Error {
  public name = 'ServiceNotFoundError';

  constructor(target: Object, propertyName: string) {
    super(
      `Cannot inject value into "${
        target.constructor.name
      }.${propertyName}". ` +
        `Please make sure you setup reflect-metadata properly and you don't use interfaces without service tokens as injection value.`,
    );
    Object.setPrototypeOf(this, CannotInjectError.prototype);
  }
}

// DO NOT USE THIS YET, BABEL 7 IS STRIPPING "design:type" (tsconfig.json is not going to work for compiling, only for editing)
export function Inject(
  typeOrName?: ((type?: any) => Function) | string | Token<any>,
): Function {
  return function(target: Object, propertyName: string) {
    if (!typeOrName) {
      typeOrName = () =>
        (Reflect as any).getMetadata('design:type', target, propertyName);
    }

    Object.defineProperty(target, propertyName, {
      get: () => {
        let identifier: any;
        if (typeof typeOrName === 'string') {
          identifier = typeOrName;
        } else if (typeOrName instanceof Token) {
          identifier = typeOrName;
        } else {
          identifier = (typeOrName as any)();
        }

        if (identifier === Object) {
          throw new CannotInjectError(target, propertyName);
        }

        return Container.get<any>(identifier);
      },
      set: (newValue) => {
        target[propertyName] = newValue;
      },
    });
  };
}

export const Injectable = Service;
export const DependencyContainer = Container;
